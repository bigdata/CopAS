#!/bin/bash

# Creator: Tom Rebok, 2020
# Team members: Tom Rebok, Tomas Svoboda, Jozef Cibik
# version 3.1
#
# Installation of Docker and CopAS
#

INSTALL_DIR="/opt/CopAS/"

echo -e "**********************************************************************************"
echo -e "* CopAS (Cops Analytic System) -- a system for data analyses using Elastic stack *"
echo -e "*       Created by Institute of Computer Science, Masaryk University, 2018       *"
echo -e "**********************************************************************************\n"


# check, whether we're an administrator
if [[ $EUID -ne 0 ]]; then
	echo >&2 "ERROR: Sorry, this installation script must be run as root!"
	echo -e >&2 "\nAborting installation..."; 
	exit 1
fi

echo "This script is going to install Docker engine (if not available) and install/update CopAS system on your computer."
# check, whether user really wants to continue
while true; do
	read -p "Would you like to continue? (Y/n) : " choice
	choice=${choice:-y}
    case $choice in
        [Yy]* ) break;;
        [Nn]* ) exit 1;;
        *) echo "Please answer 'y' (yes) or 'n' (no)...";;
    esac
done
	
# let's check the OS release and version
if [ -f /etc/os-release ]; then
	# freedesktop.org and systemd
	. /etc/os-release
	OS="$(echo $ID | tr '[:upper:]' '[:lower:]')"
	VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
	# linuxbase.org
	OS="$(lsb_release -si | tr '[:upper:]' '[:lower:]')"
	VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
	# For some versions of Debian/Ubuntu without lsb_release command
	. /etc/lsb-release
	OS="$(echo $DISTRIB_ID | tr '[:upper:]' '[:lower:]')"
	VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
	# Older Debian/Ubuntu/etc.
	OS=debian
	VER=$(cat /etc/debian_version)
else
	echo >&2 "ERROR: Unable to identify your OS and/or OS version!"
	echo -e >&2 "\nAborting installation..."; 
	exit 1;
fi


# check, whether the Docker is already installed
if hash docker 2>/dev/null; then
	if [ "$OS" == "debian" -o "$OS" == "ubuntu" ] && dpkg -l docker.io >/dev/null 2>/dev/null ; then
		echo -e "\nERROR: Your Docker version seems to be a bit OLD."
		echo -e "If not required, please, uninstall it using 'sudo apt-get remove docker docker-engine docker.io' and run the installation once again.\n"
		exit 1
	elif [ "$OS" == "fedora" -o "$OS" == "centos" ] && yum list installed docker docker-engine >/dev/null 2>/dev/null ; then
		echo -e "\nERROR: Your Docker version seems to be a bit OLD."
		echo -e "If not required, please, uninstall it using 'sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-selinux docker-engine-selinux docker-engine' and run the installation once again.\n"
		exit 1
	fi
	echo -e "\nINFO: Docker seems to be already installed -- skipping its installation...\n"
else
	# check, whether network is available
	if ! ping -c1 -w3 8.8.8.8 >/dev/null 2>/dev/null; then
		echo >&2 "ERROR: Network connection seems to be down!"
		echo >&2 "       Please, check the access to the internet."
		echo -e >&2 "\nAborting installation..."; 
		exit 1;
	fi

	# if there're available tools, let's install Docker
	# generic variants:  "wget -qO- https://get.docker.com/ | sh"  OR  "curl -sSL https://get.docker.com/ | sh"

	# let's install Docker depending on the OS:
	case "$OS" in
		"debian")
			echo "CopAS: Updating the package index ... "
			apt-get update >/dev/null 2>/dev/null
			echo "DONE"

			echo "CopAS: Installing packages to allow apt to use a repository over HTTPS: ... "
			if [ `echo $VER | cut -d. -f1` -le 7 ]; then
				# for Wheezy (7.x) and olders
				apt-get --yes install apt-transport-https ca-certificates curl python-software-properties
			else
				# newer versions
				apt-get --yes install apt-transport-https ca-certificates curl gnupg2 software-properties-common bind9-host dnsutils
			fi
			echo "DONE"

			echo "CopAS: Adding Docker's official GPG key ... "
			curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
			echo "DONE"

			echo "Adding Docker's repository ... "
			add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
			# On Wheezy, the add-apt-repository adds deb-src as well, which does not exist. We have to remove it...
			if [ `echo $VER | cut -d. -f1` -eq 7 ]; then
				# for Wheezy (7.x) only
				sed -e '/deb-src.*download.*docker.*wheezy.*stable/ s/^#*/#/' -i /etc/apt/sources.list
			fi
			echo "DONE"

			echo "CopAS: Updating index and installing Docker ... "
			apt-get update >/dev/null 2>/dev/null
			apt-get --yes install docker-ce
			echo "DONE"
			;;
		"ubuntu")
			if [ `echo $VER | cut -d. -f1` -le 14 ]; then
				echo >&2 "ERROR: Installation on Ubuntu 14.x and older is not supported!"
				echo -e >&2 "\nAborting installation..."; 
				exit 1;
		    fi
			
			echo "CopAS: Updating the package index ... "
			apt-get update >/dev/null 2>/dev/null
			echo "DONE"

			echo "CopAS: Installing packages to allow apt to use a repository over HTTPS: ... "
			apt-get --yes install apt-transport-https ca-certificates curl gnupg-agent software-properties-common bind9-host bind9-dnsutils
			echo "DONE"

			echo "CopAS: Adding Docker's official GPG key ... "
			curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
			echo "DONE"

			echo "Adding Docker's repository ... "
			add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
			echo "DONE"

			echo "CopAS: Updating index and installing Docker ... "
			apt-get update >/dev/null 2>/dev/null
			apt-get --yes install docker-ce docker-ce-cli containerd.io
			echo "DONE"
			;;
		"centos")
			echo "CopAS: Installing packages yum-utils, device-mapper-persistent-data and lvm2 ... "
			yum install -y yum-utils device-mapper-persistent-data lvm2 >/dev/null 2>/dev/null
			echo "DONE"

			echo "Adding Docker's repository ... "
			yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
			echo "DONE"

			echo "CopAS: Istalling Docker ... "
			yum install -y docker-ce
			echo "DONE"
			
			echo "CopAS: Starting Docker ... "
			systemctl start docker
			echo "DONE"
			;;
		"fedora")
			echo "CopAS: Installing package dnf-plugins-core ... "
			dnf -y install dnf-plugins-core >/dev/null 2>/dev/null
			echo "DONE"

			echo "Adding Docker's repository ... "
			dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
			echo "DONE"

			echo "CopAS: Istalling Docker ... "
			dnf install docker-ce
			echo "DONE"
			
			echo "CopAS: Starting Docker ... "
			systemctl start docker
			echo "DONE"
			;;
		*)
			echo >&2 "ERROR: Unsupported OS ($OS) and/or version ($VER)! Please, contact CopAS maintainers."
			echo -e >&2 "\nAborting installation..."; 
			exit 1;
	esac

	# let's adapt the Docker MTU based on host's MTU
	# we have to find out which interface is the default one
	if ! hash ip 2>/dev/null ; then
		echo "ERROR: It seems, that 'ip' command is NOT available! Unable to set the Docker daemon MTU."
		echo "Exiting...";
		exit 1;
	else
		default_iface_name="`ip route show default | sed 's/.*dev //'`"
		default_iface_mtu="`ip link show dev $default_iface_name | grep "mtu" | sed 's/.*mtu \([0-9]\+\) .*/\1/'`"

		# create the appropriate config file for Docker daemon
cat >/etc/docker/daemon.json <<EOF
{
"mtu": $default_iface_mtu
}
EOF
	
		# finally, we have to restart the daemon (TODO: distribution specific?)
		systemctl restart docker
	fi

	# finalize with some information
	echo -e "\n\n-----------------------------------------------------------------------------"
	echo -e "\nSUCCESS: Docker subsystem has been successfully installed."
	echo -e "\n\nIMPORTANT: To allow common users to run Docker containers, add the relevant users"
	echo -e "to the 'docker' system group (edit /etc/groups or run the command(s) 'usermod -a -G docker USERNAME' for each user).\n"
	echo -e "-----------------------------------------------------------------------------\n\n"
fi


# check, whether CopAS is already installed...
if [ -f "$INSTALL_DIR/bin/copas" -a -f "$INSTALL_DIR/etc/Dockerfile" ]; then
	echo "WARNING: CopAS seems to be already installed."

	while true; do
		read -p "Would you like to update it? (Y/n) : " choice
		choice=${choice:-y}
		case $choice in
			[Yy]* ) break;;
			[Nn]* ) echo -e "\nFINISH: Skipping CopAS update...\n" ; exit 0;;
			*) echo "Please answer 'y' (yes) or 'n' (no)...";;
		esac
	done
fi

SCRIPT_DIR="`dirname $0`"

# copy-out the CopAS start/stop files and config files to $INSTALL_DIR/{bin,etc} directory
if ! [ -d "$SCRIPT_DIR/bin" -o -d "$SCRIPT_DIR/etc" -o -f "$SCRIPT_DIR/bin/copas" -o -f "$SCRIPT_DIR/etc/Dockerfile" ]; then
	echo -e >&2 "\nERROR: CopAS startup and/or configuration files NOT available! Not installing CopAS...";
	exit 1
else
	mkdir -p "$INSTALL_DIR"

	if [ $? -eq 0 ]; then
		cp -r "$SCRIPT_DIR/bin" "$SCRIPT_DIR/etc" "$INSTALL_DIR"
		chmod a+x "$INSTALL_DIR"/bin/*
		ln -f -s "$INSTALL_DIR/bin/copas" /usr/local/bin/
		mkdir -p "$INSTALL_DIR/datastore/"
		chgrp -R docker "$INSTALL_DIR/datastore/"
		chmod -R g+w "$INSTALL_DIR/datastore/"
	else
		echo -e >&2 "\nWARNING: Unable to create the '$INSTALL_DIR' directory. Not copying CopAS files...";
	fi
fi
	
echo -e "\nFINISH: CopAS installation/update finished...\n"

exit 0
