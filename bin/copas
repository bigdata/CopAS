#!/bin/bash

# Creator: Tom Rebok, 2018
# Team members: Tom Rebok, Tom Svoboda
#
# Main script for controlling CopAS system
#

COPAS_VERSION="2018.07"
COPAS_IMAGE="copasimg"
COPAS_BASEDIR="/opt/CopAS/"


#### COMMON FUNCTIONS ####

usage()
{
    echo "Usage: `basename $0` ACTION [OPTIONS] [container_name]" 
    echo "       Available actions:" 
    echo "          create  ... creates a CopAS container (named 'container_name', if provided)" 
    echo "          start   ... starts a CopAS container (named 'container_name', if provided)" 
    echo "          stop    ... stops a CopAS container (named 'container_name', if provided)" 
    echo "          destroy ... destroys a CopAS container (named 'container_name', if provided)" 
    echo "                      -p|--purge option ... delete all the container data" 
    echo "                      -k|--keep-data option ... keep all the container data" 
    echo "          info    ... shows information about available CopAS containers" 
    echo "          monitor ... monitors the resource usage of CopAS containers" 
    echo "                      -l|--live option ... show the live resource usage" 
    echo "          enter   ... enters a CopAS container (named 'container_name', if provided)" 
    echo "          update  ... updates the CopAS base image" 
    echo "                      if a filename is provided, updates from the local image" 
    echo "          backup  ... backup the CopAS container (named 'container_name', if provided)" 
    echo "          load    ... load the previously backed up container from a file" 
    echo "                      if a filename is provided, loads from the specified file" 
    echo "          version ... shows the versions of CopAS and containers' image" 
    echo "          debug   ... exports a file with debug information for CopAS maintainers" 
    exit 1
}

list_containers()
{
	CopAS_containers="`docker ps -a -f label=copas.id -q`"
	if [ -z "$CopAS_containers" ]; then
		echo "    NO CopAS container available..."
	else
		printf "    %-14s %-12s %-40s %s\n" "Name" "User" "URL" "State"
		echo "    ============================================================================"
		for container in $CopAS_containers; do
			printf "    %-14s %-12s %-40s %s\n" $(docker inspect --format '{{ index .Config.Labels "copas.name" }} {{ index .Config.Labels "copas.user" }} http://HOSTNAME:{{ (index (index .HostConfig.PortBindings "8080/tcp") 0).HostPort }} {{ .State.Running }}' $container | sed -e "s/HOSTNAME/$COPAS_SERVER_HOSTNAME/" -e "s/false$/stopped/" -e "s/true$/RUNNING/")
		done
	fi
}

choose_container()
{
	# $1 ... action
	# $2 ... container name (if provided on command line)
	# Returns the name of the chosen container (textual form written in global variable 'container_name')
	
	if [ -n "$2" ]; then
		# container name has been provided
		choice="$2"
	else
		CopAS_containers="`docker ps -a -f label=copas.id -q`"
		if [ -z "$CopAS_containers" ]; then
			echo "There's NO CopAS container available..."

			if [ "$1" == "destroy" ]; then
				echo "Exiting..."
				exit 1
			fi

			while true; do
				read -p "Would you like to create one? (Y/n) : " choice
				choice=${choice:-y}
				case $choice in
					[Yy]* ) 
							$0 create && exec $0 $1
							;;
					[Nn]* ) echo "NOT ${1}ing the container, exiting..." ; exit 1;;
					*) echo "Please answer 'y' (yes) or 'n' (no)...";;
				esac
			done
		elif [ `echo "$CopAS_containers" | wc -l` -eq 1 ]; then
			# just a single one is available, get its name
			choice="`docker inspect --format='{{ index .Config.Labels "copas.name" }}' "$CopAS_containers"`"
		else
			echo -e "There are several CopAS containers available:"
			list_containers
			
			echo
			choice=""
			while true; do
				read -e -p "Which one would you like to $1? (write its NAME) : " choice
				if [ -n "`docker ps -a -q -f name="$choice"`" ]; then
					break;
				else
					echo "ERROR: The container named '$choice' is NOT available. Please, provide the correct name..."
				fi
			done
		fi
	fi

	# check, whether the provided container is available
	if [ -z "`docker ps -a -q -f name="$choice"`" ]; then 
		echo "ERROR: The container named '$choice' is NOT available."
		echo "Exiting..."
		exit 1
	fi
	
	container_name="$choice"
}

update_container_metadata()
{
	# $1 ... container name
	# $2 ... metadata identifier
	# $3 ... metadata identifier value
	# Return code determines, whether setting the metadata was successful.

	# Test, whether the metadata file is accessible
	if ! `touch "$COPAS_BASEDIR/datastore/$1/.metadata" 2>/dev/null` ; then
		echo "ERROR: The container metadata file is not accessible! Please, check, whether container directory in datastore exists."
		echo "Exiting..."
		exit 1
	fi

	# update the value
	sed -i "/^$2=/d" "$COPAS_BASEDIR/datastore/$1/.metadata" && echo "$2=$3" >>"$COPAS_BASEDIR/datastore/$1/.metadata"
}

get_container_metadata()
{
	# $1 ... container name
	# $2 ... metadata identifier
	# Returns the metadata value of a specified container.

	# Test, whether the metadata file is accessible
	if ! [ -f "$COPAS_BASEDIR/datastore/$1/.metadata" ]; then
		echo "ERROR: The container metadata file is not accessible!"
		echo "Exiting..."
		exit 1
	fi

	# get the value
	retval=`grep "^$2=" "$COPAS_BASEDIR/datastore/$1/.metadata" | cut -d'=' -f2`

	if [ -z "$retval" ]; then
		echo "N/A"
	else
		echo "$retval"
	fi
}

clean_container_metadata()
{
	# $1 ... container name
	# Return code determines, whether cleaning the metadata was successful.

	# Test, whether the metadata file is accessible
	if ! [ -f "$COPAS_BASEDIR/datastore/$1/.metadata" ]; then
		echo "ERROR: The container metadata file is not accessible!"
		echo "Exiting..."
		exit 1
	fi

	# clean the metadata
	echo >"$COPAS_BASEDIR/datastore/$1/.metadata"
}


#### INIT CHECK ####

# running under administrator is not secure...
if [[ $EUID -eq 0 ]]; then
	echo "WARNING: Running under administrator/root is NOT secure!"

	while true; do
		read -p "Would you like to continue? (Y/n) : " choice
		choice=${choice:-y}
		case $choice in
			[Yy]* ) break;;
			[Nn]* ) echo "Exiting..." ; exit 1;;
			*) echo "Please answer 'y' (yes) or 'n' (no)...";;
		esac
	done
fi

# check, whether the Docker is installed
if ! hash docker 2>/dev/null; then
	echo "ERROR: It seems, that Docker is NOT installed!"
	echo "Exiting..."; 
	exit 1;
fi

# do we have the right getopt for parsing arguments?
getopt --test >/dev/null 2>/dev/null
if [[ $? -ne 4 ]]; then
	echo "ERROR: The necessary version of 'getopt' is NOT installed ('getopt --test' failed in this environment)."
	echo "\nExiting..."; 
	exit 1
fi

# let's discover our host identification
if ! hash host 2>/dev/null || ! hash dig 2>/dev/null ; then
	echo "ERROR: It seems, that 'host' or 'dig' command is NOT available!"
	echo "Exiting...";
	exit 1;
else
	# overall: let's identify the server hostname or IP address
	
    # let's try using DNS lookup (external service)
	# for OpenStack, this should also work: ec2metadata --public-ipv4
	default_iface_ip="`dig @resolver1.opendns.com ANY myip.opendns.com +short -4`"
	if [ -z "$default_iface_ip" ]; then
		# not found; let's get the IP address of the default interface
		default_iface=$(awk '$2 == 00000000 { print $1 ; exit }' /proc/net/route)
		default_iface_ip=$(ip addr show dev "$default_iface" | awk '$1 == "inet" { sub("/.*", "", $2); print $2 }')
	fi

    # do we have a hostname available? If so, let's use it... ("head -n1" because of multiple hostnames, like in OpenStack)
    if default_iface_ip_dns=`host $default_iface_ip | head -n1`; then
		COPAS_SERVER_HOSTNAME="`echo "$default_iface_ip_dns" | awk '/domain name pointer/ { print substr($5, 1, length($5)-1) }'`"
	else
		COPAS_SERVER_HOSTNAME="$default_iface_ip"
	fi
fi



#### PARSING ARGUMENTS ####

action=`echo $1 | tr '[A-Z]' '[a-z]'`
shift

case "$action" in
	"create")
			# parse options
			parsed=$(getopt -o "" --longoptions "image:" --name "$0" -- "$@")
			if [[ $? -ne 0 ]]; then
				usage
				exit 1
			fi
			
			# use eval with "$parsed" to properly handle the quoting
			eval set -- "$parsed"

			while true; do
				case "$1" in
					--image)
						COPAS_IMAGE="$2"
						shift 2
						;;
					--)
						shift ; break ;;
					*)
						break ;;
				esac
			done

			# find a free container ID
			# get all the used IDs ("x" is because of following egrep, which doesn't match numbers)
			IDs_used="x"
			for container in `docker ps -a -f label=copas.id -q` ; do
				IDs_used="$IDs_used|`docker inspect --format='{{ index .Config.Labels "copas.id" }}' $container`"
			done
			container_id=`seq 1 100 | egrep -v -e "$IDs_used" | head -n1`

			# choose container name (if invalid, ask for another)
			while true; do
				if [ -n "$1" ]; then
					container_name="$1"
					shift
				else
					read -e -p "Please, provide container NAME (default 'copas${container_id}') : " container_name
					container_name="${container_name:-copas${container_id}}"
				fi

				# check, whether the container name is valid (Do you know a better way? Note that 'a-z' contains accented chars :( )
				if ! [[ "$container_name" =~ ^[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-]+$ ]]; then
					echo -e "ERROR: Invalid name for CopAS container ('$container_name')!\n" 
				elif [ -n "`docker ps -a -q -f name="$container_name"`" ]; then 
					# check whether a container with this name already exists
					echo -e "WARNING: CopAS container named '$container_name' already EXISTS!\n" 
				else
					break
				fi
			done

			# check, whether the CopAS image is already created; if not, create it
			if [ -z "`docker images -q "$COPAS_IMAGE" 2>/dev/null`" ]; then 
				echo -e "\nWell, the container image has not been created yet..."

				while true; do
					read -p "Should I build it? (Y/n) : " choice
					choice=${choice:-y}
					case $choice in
						[Yy]* ) $0 buildimg --image="$COPAS_IMAGE" || exit 1 ; break ;;
						[Nn]* ) echo "NOT building the container image, exiting..." ; exit 1;;
						*) echo "Please answer 'y' (yes) or 'n' (no)...";;
					esac
				done
			fi
			
			# create the container
			echo -en "\nCreating the CopAS container '$container_name' : "

			# create the container data directory
			if [ -d "$COPAS_BASEDIR/datastore/$container_name" ]; then
				echo -e "\nWARNING: The container data directory '$COPAS_BASEDIR/datastore/$container_name' already exists! Using it..."
			else
				mkdir -p "$COPAS_BASEDIR/datastore/$container_name"
			fi
			
			docker create -l copas.name="$container_name" -l copas.id="$container_id" -l copas.user="`whoami`" --env COPAS_NAME="$container_name" --env COPAS_ID="$container_id" --env COPAS_HOST="$COPAS_SERVER_HOSTNAME" --env COPAS_VERSION="$COPAS_VERSION" -p `expr 5600 + $container_id`:5601 -p `expr 8004 + $container_id`:8005 -p `expr 8080 + $container_id`:8080 -it --name "$container_name" --hostname "$container_name" --volume "$COPAS_BASEDIR/datastore/$container_name":/data-shared --ulimit memlock=-1:-1 --ulimit nofile=65536:65536 --ulimit nproc=8192:8192 --privileged $COPAS_IMAGE >/dev/null

			if [ $? -eq 0 ]; then
				echo "DONE"
			else
				echo "FAIL"
			fi
		;;
	"start")
			# choose, which container we should work with
			choose_container start "$1"

			# check, whether the container is running
			if [ "`docker inspect --format="{{.State.Running}}" "$container_name" 2>/dev/null`" == "true" ]; then 
				echo "WARNING: The CopAS container '$container_name' is already running."
				echo -e "\nExiting...\n" 
				list_containers
			else
				echo -n "Starting the CopAS container '$container_name' ... "
				docker start "$container_name" >/dev/null

				if [ $? -eq 0 ]; then
					# if previously backed-up, remove the indication
					update_container_metadata "$container_name" "CONTAINER_BACKUP" "NO"

					echo DONE
					echo -e "\nContainer URL: http://$COPAS_SERVER_HOSTNAME:`docker inspect --format '{{ (index (index .HostConfig.PortBindings "8080/tcp") 0).HostPort }}' $container_name`"
					echo "(Please, be patient -- wait a minute before going to the URL. Services inside the container are starting...)"
				else
					echo FAIL
				fi
			fi
		;;
	"enter")
			# choose, which container we should work with
			choose_container enter "$1"

			# check, whether the container is running
			if [ "`docker inspect --format="{{.State.Running}}" "$container_name" 2>/dev/null`" == "false" ]; then 
				echo -e "WARNING: The CopAS container '$container_name' is NOT RUNNING!"
				
				while true; do
					read -p "Would you like to start it? (Y/n) : " choice
					choice=${choice:-y}
					case $choice in
						[Yy]* ) $0 start "$container_name" || exit 1 ; break;;
						[Nn]* ) echo "NOT starting the container, exiting..."; exit 1;;
						*) echo "Please answer 'y' (yes) or 'n' (no)...";;
					esac
				done
			fi
				
			echo -e "\nEntering the CopAS container '$container_name' ... "

			# entering the CopAS container...
			docker exec -it "$container_name" "/bin/bash" 
		;;
	"stop")
			# choose, which container we should work with
			choose_container stop "$1"

			# check, whether the container is running
			if [ "`docker inspect --format="{{.State.Running}}" "$container_name" 2>/dev/null`" == "false" ]; then 
				echo -e "WARNING: The CopAS container '$container_name' is NOT RUNNING!"
				echo -e "\nExiting...\n" 
				list_containers
			else
				echo -n "Stopping the CopAS container '$container_name' ... "
				docker stop "$container_name" >/dev/null

				if [ $? -eq 0 ]; then
					echo DONE
				else
					echo FAIL
				fi
			fi
		;;
	"destroy")
			# parse options
			parsed=$(getopt -o "pk" --longoptions "purge,keep-data" --name "$0" -- "$@")
			if [[ $? -ne 0 ]]; then
				usage
				exit 2
			fi
			# use eval with "$parsed" to properly handle the quoting
			eval set -- "$parsed"

			delete_data="ask"
			while true; do
				case "$1" in
					-p|--purge)
						delete_data="true"
						shift
						;;
					-k|--keep-data)
						delete_data="false"
						shift
						;;
					--)
						shift ; break ;;
					*)
						break ;;
				esac
			done

			# lets choose, which container we should work with
			choose_container destroy "$1"

			# check, whether the container is running
			if [ "`docker inspect --format="{{.State.Running}}" "$container_name" 2>/dev/null`" == "true" ]; then 
				echo -e "WARNING: The CopAS container '$container_name' is RUNNING!"
				
				while true; do
					read -p "Would you like to stop it? (y/N) : " choice
					choice=${choice:-n}
					case $choice in
						[Yy]* ) $0 stop "$container_name" || exit 1; break;;
						[Nn]* ) echo "NOT stopping the container, exiting..."; exit 1;;
						*) echo "Please answer 'y' (yes) or 'n' (no)...";;
					esac
				done
			fi

			# Is the container backed up?
			if [ "`get_container_metadata "$container_name" "CONTAINER_BACKUP"`" != "YES" ]; then
				echo -e "\nWARNING: Your container is NOT BACKED up!!!\n"
				sleep 1
			fi
			
			# are you sure about deleting the container?
			while true; do
				read -p "Would you really like to DESTROY the container '$container_name'? (y/N) : " choice
				choice=${choice:-n}
				case $choice in
					[Yy]* ) break;;
					[Nn]* ) echo "NOT destroying the container, exiting..."; exit 1;;
					*) echo "Please answer 'y' (yes) or 'n' (no)...";;
				esac
			done

			echo -n "Destroying the CopAS container '$container_name' ... "
			
			exit_status=0
			container_image="`docker inspect --format="{{.Config.Image}}" "$container_name" 2>/dev/null`"

			# remove the container
			docker rm "$container_name" >/dev/null || exit_status=$?

			# if this is the last container using the particular image (different from the basic one), delete the image as well...
			if [ "$container_image" != "$COPAS_IMAGE" ] && [ -z "`docker ps -a -f ancestor="$container_image" -q`" ]; then
				docker rmi "$container_image" >/dev/null
			fi

			# finally, delete the data directory
			if [ $exit_status -eq 0 ] && [ -d "$COPAS_BASEDIR/datastore/$container_name" ]; then
				if [ -z "`find "$COPAS_BASEDIR/datastore/$container_name" -type f -not -name ".*" -print -quit 2>/dev/null`" ]; then
					# there're no files in the container datadir
					rm -rf "$COPAS_BASEDIR/datastore/$container_name" || exit_status=$?
				else
					# there're some data in the container datadir
					if [ "$delete_data" == "ask" ]; then 
						echo -e "\nWARNING: The container data directory '$COPAS_BASEDIR/datastore/$container_name' contains some data..."
						while true; do
							read -p "Would you like to DELETE them? (y/N) : " choice
							choice=${choice:-n}
							case $choice in
								[Yy]* ) delete_data="true"; break;;
								[Nn]* ) delete_data="false"; break;;
								*) echo "Please answer 'y' (yes) or 'n' (no)...";;
							esac
						done
					fi
				
					if [ "$delete_data" == "true" ]; then
						rm -rf "$COPAS_BASEDIR/datastore/$container_name" || exit_status=$?
						if [ $exit_status -eq 0 ]; then
							echo "INFO: Container data directory sucessfully deleted..."
						else
							echo "ERROR: UNABLE to delete the container data directory. Please, delete it manually..."
						fi
					else
						clean_container_metadata "$container_name"
						echo "INFO: NOT deleting the container data directory..."
					fi
				fi
			fi

			if [ $exit_status -eq 0 ]; then
				echo DONE
			else
				echo FAIL
			fi
		;;
	"update")
			# parse options
			parsed=$(getopt -o "" --longoptions "image:" --name "$0" -- "$@")
			if [[ $? -ne 0 ]]; then
				usage
				exit 2
			fi
			# use eval with "$parsed" to properly handle the quoting
			eval set -- "$parsed"

			while true; do
				case "$1" in
					--image)
						COPAS_IMAGE=$2
						shift 2
						;;
					--)
						shift ; break ;;
					*)
						break ;;
				esac
			done

			# if there is a container using that image created, exit (require to destroy it)
			if [ -n "`docker ps -a -f ancestor="$COPAS_IMAGE" -q`" ]; then 
				echo -e "ERROR: There is a CopAS container using the image -- unable to update the containers' image." 
				echo -e "       Please, check the following one(s) and destroy the container(s) first:\n" 
				echo -e "\nCopAS containers:\n*****************"
				list_containers
				echo -e "\n\nAll Docker containers:\n**********************"
				docker ps -a -f ancestor="$COPAS_IMAGE"
				echo -e "\nExiting..." 
				exit 1
			fi

			# check, whether the image is already installed
			if [ -n "`docker images -q "$COPAS_IMAGE" 2>/dev/null`" ]; then 
				echo "There is a CopAS image ('$COPAS_IMAGE') already created..."
				
				while true; do
					read -p "Would you really like to destroy and UPDATE it? (y/N) : " choice
					choice=${choice:-n}
					case $choice in
						[Yy]* ) docker rmi "$COPAS_IMAGE" >/dev/null
								if [ $? -eq 0 ]; then
									echo "CopAS image successfully destroyed..."
									break
								else
									echo "ERROR: Unable to destroy the CopAS image! Please, destroy the image manually using Docker functions."
									echo "Exiting..."
									exit 1
								fi
								;;
						[Nn]* ) echo "NOT destroying the image, exiting..."; exit 1;;
						*) echo "Please answer 'y' (yes) or 'n' (no)...";;
					esac
				done
			fi

			# there's no image -- let's create/import a new one...
			$0 buildimg --image="$COPAS_IMAGE" $1
		;;
	"backup")
			backup_data="ask"

			# lets choose, which container we should work with
			choose_container backup "$1"

			# check, whether the container is running
			if [ "`docker inspect --format="{{.State.Running}}" "$container_name" 2>/dev/null`" == "true" ]; then 
				echo -e "WARNING: The CopAS container '$container_name' is RUNNING!"
				
				while true; do
					read -p "Would you like to stop it before backup? (y/N) : " choice
					choice=${choice:-n}
					case $choice in
						[Yy]* ) $0 stop "$container_name" || exit 1; break;;
						[Nn]* ) echo "NOT stopping the container, exiting without backup..."; exit 1;;
						*) echo "Please answer 'y' (yes) or 'n' (no)...";;
					esac
				done
			fi
				
			read -e -p "Please, provide the NAME of your container backup (default '$container_name-`date +%Y%m%d`') : " backupname
			backupname=${backupname:-$container_name-`date +%Y%m%d`}
			if [ -e "${backupname}.img" -o -e "${backupname}.data" ]; then
				echo "WARNING: The image file '${backupname}.img' OR the data file '${backupname}.data' already exist!"
				while true; do
					read -p "Overwrite? (y/N) : " choice
					choice=${choice:-n}
					case $choice in
						[Yy]* ) break;;
						[Nn]* ) echo "SKIPPING the container backup, exiting..."; exit 1;;
						*) echo "Please answer 'y' (yes) or 'n' (no)...";;
					esac
				done
			fi
			
			# should we backup the data as well?
			if [ "$backup_data" == "ask" ] && [ -n "`find "$COPAS_BASEDIR/datastore/$container_name" -type f -not -name ".*" -print -quit 2>/dev/null`" ]; then
				echo -e "\nWarning: The container data directory is NOT empty."
				while true; do
					read -p "Should I backup the data as well? (y/N) : " choice
					choice=${choice:-n}
					case $choice in
						[Yy]* ) backup_data="true"; break;;
						[Nn]* ) backup_data="false"; break;;
						*) echo "Please answer 'y' (yes) or 'n' (no)...";;
					esac
				done
			fi

			if [ "$backup_data" == "true" ]; then
				echo -en "\nBackuping the container '$container_name' and its data into the files '${backupname}.img' and '${backupname}.data' ... "
			else
				echo -en "\nBackuping the container '$container_name' (WITHOUT data) into the file '${backupname}.img' ... "
			fi

			echo -n "CONTAINER "
			# let's create a new image with all the container changes (neccessary when for import with different name) and then export it...
			exit_status=0
			tempimage_name="copasbackup/$container_name:`cat /dev/urandom | tr -dc "[:alpha:]" | head -c 6`"
			docker commit -m "name: $container_name, date: `date +"%d.%m.%Y %R"`, host: $COPAS_SERVER_HOSTNAME, user: `whoami` ($(getent passwd $(whoami) | cut -d':' -f5 | tr -d ',')), CopAS version: $COPAS_VERSION, Docker version: `docker --version`" "$container_name" "$tempimage_name" >/dev/null || exit_status=$?
			docker save --output "${backupname}.img" "$tempimage_name" >/dev/null || exit_status=$?
			docker rmi "$tempimage_name" >/dev/null || exit_status=$?
			echo -n "(done)"

			# export data as well (if requested)
			if [ "$backup_data" == "true" ]; then
				echo -n ", DATA "
				# without verification (--verify), compressed files cannot be verified
				tar czf "${backupname}.data" --one-file-system --directory "$COPAS_BASEDIR/datastore/" "$container_name" || exit_status=$?
				echo -n "(done)"
			fi

			if [ $exit_status -eq 0 ]; then
				update_container_metadata "$container_name" "CONTAINER_BACKUP" "YES"
				echo -e "\nDONE"
			else
				echo -e "\nFAIL"
			fi
		;;
	"load")
			# parse options
			parsed=$(getopt -o "" --longoptions "image:" --name "$0" -- "$@")
			if [[ $? -ne 0 ]]; then
				usage
				exit 2
			fi
			# use eval with "$parsed" to properly handle the quoting
			eval set -- "$parsed"

			while true; do
				case "$1" in
					--image)
						COPAS_IMAGE="$2"
						shift 2
						;;
					--)
						shift ; break ;;
					*)
						break ;;
				esac
			done

			if [ -n "$1" ]; then
				# let's support specification with or without extension
				container_imagefile_base="`echo $1 | sed 's/\.img$//'`"
				if [ -f "${container_imagefile_base}.img" ]; then
					container_imagefile="$container_imagefile_base"
				else
					echo "ERROR: The provided filename '$1' does not exist! Exiting..."
					exit 1
				fi
			else
				# let's ask the user, no filename has been provided
				while true; do
					read -e -p "Please, provide the FILENAME you want to import the container from : " container_imagefile
					# let's support specification with or without extension
					container_imagefile_base="`echo $container_imagefile | sed 's/\.img$//'`"
					if [ ! -f "${container_imagefile_base}.img" ]; then
						echo "ERROR: The provided filename '$container_imagefile' does not exist!"
					else
						copas_imagefile="$container_imagefile_base"
						break
					fi
				done
			fi

			# get image information
			image_manifest="`tar -xf "${container_imagefile_base}.img" --to-stdout manifest.json 2>/dev/null`"
			image_container_config="`echo $image_manifest | sed 's/.*"Config":"\([^"]*\).*/\1/'`"
			image_container_imagename="`echo $image_manifest | sed 's/.*"RepoTags":\["\([^"]*\).*/\1/'`"
			image_container_comment="`tar -xf "${container_imagefile_base}.img" --to-stdout $image_container_config 2>/dev/null | sed -e 's/.*"comment":"\([^"]*\).*/\1/' -e 's/: /:/g' -e 's/, /\n/g'`"

			if [ -z "$image_container_comment" ]; then
				echo -e "ERROR: The container image file '${container_imagefile_base}.img' does not contain necessary information. Is that really a CopAS container???" 
				echo -e "\nExiting..." 
				exit 1
			fi

			# OK, we should try to load it
			image_container_name="`echo "$image_container_comment" | grep "^name:" | cut -d ':' -f2`"
			echo "You are about to load the following container:"
			echo "  Container name : $image_container_name"
			echo "  Image file     : ${container_imagefile_base}.img"
			[ -e "${container_imagefile_base}.data" ] && echo "  Data file      : ${container_imagefile_base}.data"
			echo "  Creation date  : `echo "$image_container_comment" | grep "^date:" | sed 's/^date://'`"
			echo "  Created by     : `echo "$image_container_comment" | grep "^user:" | cut -d ':' -f2`"
			echo "  On host        : `echo "$image_container_comment" | grep "^host:" | cut -d ':' -f2`"
				
			while true; do
				read -p "Would you like to continue? (Y/n) : " choice
				choice=${choice:-y}
				case $choice in
					[Yy]* ) break;;
					[Nn]* ) echo "INFO: SKIPPING the container load, exiting..."; exit 1;;
					*) echo "Please answer 'y' (yes) or 'n' (no)...";;
				esac
			done
			
			while true; do
				read -p "Would you like to import the container DATA as well? (Y/n) : " choice
				choice=${choice:-y}
				case $choice in
					[Yy]* ) load_data="true"; break;;
					[Nn]* ) echo "INFO: SKIPPING the container data..."; load_data="false"; break;;
					*) echo "Please answer 'y' (yes) or 'n' (no)...";;
				esac
			done
			
			
			# test, whether the image is already imported or not
			load_image="true"
			if [ -n "`docker images -q "$image_container_imagename" 2>/dev/null`" ]; then 
				echo -e "WARNING: The container image '${image_container_imagename}' is already imported!" 
				while true; do
					read -p "Would you like to REWRITE it? (y/N) : " choice
					choice=${choice:-n}
					case $choice in
						[Yy]* ) load_image="true"; break;;
						[Nn]* ) echo "INFO: Using already loaded image..."; load_image="false"; break;;
						*) echo "Please answer 'y' (yes) or 'n' (no)...";;
					esac
				done
			fi

			# Import the image
			if [ "$load_image" == "true" ]; then 
				echo -ne "\nImporting the container image ... "
				docker load --input ${container_imagefile_base}.img --quiet >/dev/null | sed "s/.*: //" 
				if [ $? -eq 0 ]; then
					echo "DONE"
				else
					echo "FAIL"
				fi
			fi

			# Create the container
			echo -ne "\nCreating the container ... "
			$0 create --image="$image_container_imagename" "$image_container_name"
			if [ $? -eq 0 ]; then
				echo "DONE"
			else
				echo "FAIL"
				exit 1
			fi

			# Load the data
			if [ "$load_data" == "true" ]; then
				echo -ne "\nExtracting the container data ... "
				
				# since the name could change during creation (when existed), we should determine the name of the loaded container
				container_loadedID="`docker ps -a --latest -q -f ancestor="$image_container_imagename"`"
				container_name="`docker inspect --format="{{.Name}}" $container_loadedID | sed 's#^/##'`"

				overwrite_data="true"
				if [ -n "`find "$COPAS_BASEDIR/datastore/$container_name" -type f -not -name ".*" -print -quit 2>/dev/null`" ]; then
					# there're some data in the datastore
					while true; do
						read -p "Would you like to OVERWRITE the old data? (y/N) : " choice
						choice=${choice:-n}
						case $choice in
							[Yy]* ) overwrite_data="true"; break;;
							[Nn]* ) overwrite_data="false"; break;;
							*) echo "Please answer 'y' (yes) or 'n' (no)...";;
						esac
					done
				fi
				
				if [ "$overwrite_data" == "true" ]; then
					rm -rf "$COPAS_BASEDIR/datastore/$container_name/*" "$COPAS_BASEDIR/datastore/$container_name/.*" \
						&& tar xzf "${container_imagefile_base}.data" --strip-components=1 --directory "$COPAS_BASEDIR/datastore/$container_name/"

					if [ $? -eq 0 ]; then
						clean_container_metadata "$container_name"
						echo "INFO: Container data sucessfully restored..."
					else
						echo "ERROR: UNABLE to restore the container data. Please, restore them manually..."
					fi
				else
					clean_container_metadata "$container_name"
					echo "INFO: NOT restoring the container data..."
				fi
				echo "DONE"
			fi
		;;
	"buildimg")
			# parse options
			parsed=$(getopt -o "" --longoptions "image:" --name "$0" -- "$@")
			if [[ $? -ne 0 ]]; then
				usage
				exit 2
			fi
			# use eval with "$parsed" to properly handle the quoting
			eval set -- "$parsed"

			while true; do
				case "$1" in
					--image)
						COPAS_IMAGE="$2"
						shift 2
						;;
					--)
						shift ; break ;;
					*)
						break ;;
				esac
			done

			copas_imagefile=""
			if [ -n "$1" ]; then
				if [ -f "$1" ]; then
					copas_imagefile="$1"
				else
					echo "ERROR: The filename '$1' does not exist! Exiting..."
					exit 1
				fi
			else
				# let's ask the user, no filename has been provided
				echo -e "\nHow would you like to upload the new CopAS image?"
				echo "    I ... over Internet (default)"
				echo "    L ... provide Local image file"
				while true; do
					read -p "Your choice (I/L) : " choice
					choice=${choice:-i}
					case $choice in
						[Ii]* ) break
								;;
						[Ll]* ) 
								while true; do
									read -e -p "Please, provide the FILENAME you want to import the image from : " filename
									if [ ! -f "$filename" ]; then
										echo "ERROR: The file '$filename' does not exist!"
									else
										copas_imagefile="$filename"
										break
									fi
								done
								;;
						*) echo "Please answer 'I' (Internet) or 'L' (local image)...";;
					esac
					[ -n "$copas_imagefile" ] && break
				done
			fi

			if [ -z "$copas_imagefile" ]; then
				# still no filename, assume updating over Internet
				if [ -f $COPAS_BASEDIR/etc/Dockerfile ]; then
					echo "Building the image from the Internet..."

					cd $COPAS_BASEDIR/etc
					docker build -t "$COPAS_IMAGE" -f ./Dockerfile .

					if [ $? -eq 0 ]; then
						echo -e "\n\nSUCCESS: CopAS image successfully created..."
						exit 0;
					else 
						echo -e "\n\nERROR: Unable to build the CopAS image!" 
						exit 1;
					fi
				else
					echo "ERROR: Unable to initialize the CopAS image -- the necessary file (Dockerfile) is NOT available!"
					echo -e "\nAborting CopAS image build..."; 
					exit 1;
				fi
			else
				echo -en "\nUploading the image '$copas_imagefile' ... "
				
				docker load --quiet --input "$copas_imagefile" >/dev/null

				if [ $? -eq 0 ]; then
					echo "DONE"
				else
					echo "FAIL"
				fi
			fi
			;;
	"exportimg")
			# parse options
			parsed=$(getopt -o "" --longoptions "image:" --name "$0" -- "$@")
			if [[ $? -ne 0 ]]; then
				usage
				exit 2
			fi
			# use eval with "$parsed" to properly handle the quoting
			eval set -- "$parsed"

			while true; do
				case "$1" in
					--image)
						COPAS_IMAGE=$2
						shift 2
						;;
					--)
						shift ; break ;;
					*)
						break ;;
				esac
			done

			# check, whether the image is installed
			if [ -z "`docker images -q "$COPAS_IMAGE" 2>/dev/null`" ]; then 
				echo "ERROR: There's no CopAS image created. Please, create it before exporting..."
				echo "Exiting..."; 
				exit 1;
			fi
				
			read -e -p "Please, provide a FILENAME you want to export the CopAS image into (default '${COPAS_IMAGE}-`date +%Y%m%d`.tgz') : " filename
			filename=${filename:-${COPAS_IMAGE}-`date +%Y%m%d`.tgz}

			if [ -e "$filename" ]; then
				echo "Warning: The file '$filename' already exists!"
				while true; do
					read -p "Overwrite? (y/N) : " choice
					choice=${choice:-n}
					case $choice in
						[Yy]* ) break;;
						[Nn]* ) echo "SKIPPING the image export, exiting..."; exit 1;;
						*) echo "Please answer 'y' (yes) or 'n' (no)...";;
					esac
				done
			fi

			echo -en "\nExporting the CopAS image into the file '$filename' ... "
			docker save --output "$filename" "$COPAS_IMAGE" >/dev/null

			if [ $? -eq 0 ]; then
				echo "DONE"
			else
				echo "FAIL"
			fi
		;;
	"info")
			if [ -z "`docker ps -a -f label=copas.id -q`" ]; then
				echo "ERROR: There are NO CopAS containers available! Exiting..."
				exit 1
			fi

			echo -e "Listing all the CopAS containers:\n"
			list_containers
		;;
	"monitor")
			# parse options
			parsed=$(getopt -o "l" --longoptions "live" --name "$0" -- "$@")
			if [[ $? -ne 0 ]]; then
				usage
				exit 2
			fi
			# use eval with "$parsed" to properly handle the quoting
			eval set -- "$parsed"

			live_monitoring=false
			while true; do
				case "$1" in
					-l|--live)
						live_monitoring=true
						shift
						;;
					--)
						shift ; break ;;
					*)
						break ;;
				esac
			done

			if [ -z "`docker ps -f label=copas.id -q`" ]; then
				echo "ERROR: There are NO RUNNING CopAS containers available! Exiting..."
				exit 1
			fi

			if $live_monitoring; then
				docker stats $(docker ps -f label=copas.id | awk '{if (NR>1) print $NF}')
			else
				echo    "Available containers:"
				echo -e "*********************\n"
				list_containers
				echo -e "\nUsage statistics:"
				echo      "*****************"
				docker stats --no-stream $(docker ps -f label=copas.id | awk '{if (NR>1) print $NF}')
			fi
		;;
	"debug"|-d|--debug)
			read -e -p "Please, provide a FILENAME you want to export the system information into (default 'CopAS-`hostname -s`-`date +%Y%m%d`.debug') : " filename
			filename=${filename:-CopAS-`hostname -s`-`date +%Y%m%d`.debug}

			if [ -e "$filename" ]; then
				echo "Warning: The file '$filename' already exists!"
				while true; do
					read -p "Overwrite? (y/N) : " choice
					choice=${choice:-n}
					case $choice in
						[Yy]* ) break;;
						[Nn]* ) echo "SKIPPING the image export, exiting..."; exit 1;;
						*) echo "Please answer 'y' (yes) or 'n' (no)...";;
					esac
				done
			fi

			echo -en "Collecting system information and generating file '$filename' for CopAS maintainers: "

			echo -e "Date: `date`\nHostname: `hostname 2>&1`\nNetwork:\n`ping -c1 -w3 8.8.8.8 2>&1`" >"$filename"
			echo -e "\n\nCopAS information:\n=========================================\n`$0 version 2>&1`\n\n`$0 monitor 2>&1`" >>"$filename"
			echo -e "\n\nDocker and system information:\n=========================================\n`docker info 2>&1`\n`docker version 2>&1`" >>"$filename"
			echo -e "\n\nContainers:\n=========================================\n`docker ps -a 2>&1`" >>"$filename"
			echo -e "\n\nContainers' statistics:\n=========================================\n`docker stats --no-stream 2>&1`" >>"$filename"
			echo -e "\n\nImages:\n=========================================\n`docker images 2>&1`" >>"$filename"
			echo -e "\n\nContainers' details:\n=========================================\n$(docker inspect $(docker ps -aq) 2>&1)\n" >>"$filename"
			echo -e "\n\nImages' details:\n=========================================\n$(docker inspect $(docker images -q) 2>&1)\n" >>"$filename"

			if [ $? -eq 0 ]; then
				echo "DONE"
			else
				echo "FAIL"
			fi
		;;
	"version"|-v|--version)
    		echo -e "CopAS version: $COPAS_VERSION"
			
			# check, whether the CopAS image is really created; if so, print its version
			if [ -z "`docker images -q "$COPAS_IMAGE" 2>/dev/null`" ]; then
				echo -e "WARNING: No CopAS image created, unable to determine its version"
			else
    			echo -e "CopAS image version: `docker inspect --format='{{range .Config.Env}}{{println .}}{{end}}' copasimg | grep COPAS_IMAGE_VERSION | cut -d'=' -f2`"
			fi
		;;
	""|"help"|"-h"|"--help")
    		echo -e "**********************************************************************" 
    		echo -e "*       CopAS -- a system for data analyses using Elastic stack      *" 
    		echo -e "* Created by Institute of Computer Science, Masaryk University, `echo "$COPAS_VERSION" | cut -d. -f1` *" 
    		echo -e "**********************************************************************\n" 
			usage
		;;
	*)
		echo -e "ERROR: Unknown action '$action'. Exiting...\n" 
		usage
esac
